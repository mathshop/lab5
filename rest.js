function validateForm() {
    var name = document.forms["myForm"]["fname"].value;
    var email = document.forms["myForm"]["femail"].value;
    var phone = document.forms["myForm"]["fphone"].value;
    var restaurantbefore = document.forms["myForm"]["answer"].value;
    var additional = document.forms["myForm"]["fadditional"].value;
    var otherss = document.forms["myForm"]["Options"].selectedIndex;
    var contactdaym = document.forms["myForm"]["myChoicesm"].checked;
    var contactdayt = document.forms["myForm"]["myChoicest"].checked;
    var contactdayw = document.forms["myForm"]["myChoicesw"].checked;
    var contactdayth = document.forms["myForm"]["myChoicesth"].checked;
    var contactdayf = document.forms["myForm"]["myChoicesf"].checked;

    if (name == null || name == "") {
        alert("Name must be filled out");
        return false;
    }
    else if (email == "" && phone == "") {

        alert("At least an Email or Phone Number must be filled out.");
        return false;
    }
    else if (otherss == 3 && additional == "") {
        alert("If other reason for inquiry, please provide additional information.");
        return false;
    }
    else if (restaurantbefore == null || restaurantbefore == "") {
        alert("Have you been to the restaurant before, check yes or no.");
        return false;
    }
    else if (contactdaym == false && contactdayt == false && contactdayw == false && contactdayth == false && contactdayf == false) {
        alert("Please choose at least one day that is best fit to contact you.");
        return false;
    }
}
        
